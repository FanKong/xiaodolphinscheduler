# DolphinScheduler for Docker and Kubernetes

* [Start Up DolphinScheduler with Docker](https://dolphinscheduler.apache.org/en-us/docs/3.0.2/user_doc/guide/start/docker.html)
* [Start Up DolphinScheduler with Kubernetes](https://dolphinscheduler.apache.org/en-us/docs/3.0.2/user_doc/guide/installation/kubernetes.html)
